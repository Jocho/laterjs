var Later = (function(externalSettings) {
	externalSettings = externalSettings || {};

	var _later = {};

	_later.settings = {
		'useLocalStorage': false,
		'autostart': true,
	};

	var sections = {};
	var templates = {};

	var information = {
		version: '0.8.16',
		author: 'Marek J. Kolcun'
	};

	/**
	 * Returns a basic information about this library into console
	 */
	_later.info = function() {
		console.info(information);
		return information;
	};

	/**
	 * Registers a new section. Section must be an existing DOM element with a valid "id" attribute. 
	 * @param section (string) - id of a DOM element
	 * @return - whether a newly registered section or an empty object
	 */
	_later.register = function(sec) {
		if (typeof sec !== 'undefined' && sec.length) {
			sections[sec] = (section(sec));
			sections[sec].initialized(true);
			sections[sec].ctrl.restore();
			return sections[sec];
		}
		else {
			console.error('No argument inserted.');
			return {};
		}
	};

	/**
	 * Unregisters the section in app. All its values and variables will be dumped
	 * @param section (string) - name of the registered section that is going to be removed
	 */
	_later.free = function(sec) {
		if (typeof sections[sec] !== 'undefined') {
			delete sections[sec];
		}
	};

	/**
	 * Initializes all the sections in current application. Can be started anytime.
	 * @param innerNode (string) - name of the section that will be (re)initiated.
	 */
	_later.init = function(innerNode) {
		var content = (typeof innerNode !== 'undefined') ? innerNode : document;
		var ids = content.querySelectorAll('[data-ljs-s]');
		
		_later.loadTemplates();
		
		for (var i in ids) {
			if (ids[i] instanceof HTMLElement) {
				if (typeof ids[i].querySelector === 'function' && ids[i].querySelector('[data-ljs-s]') != null && ids[i].querySelector('[data-ljs-s]').length) {
					init(content);
				}
				else {
					if (typeof ids[i].dataset !== 'undefined') {
						if (typeof ids[i].dataset['ljsS'] !== 'undefined') {
							this.register(ids[i].dataset['ljsS']);
						}
					}
					else if (ids[i].getAttribute('data-ljs-s') != null) {
						var ljs = ids[i].getAttribute('data-ljs-s');
						if (ljs != null && ljs.length) {
							this.register(ljs);
						}
					}
				}
			}
		}
	};

	/**
	 * When inserting an object with a key-value pairs where keys exist in Later's setup object, those setup values will be updated.
	 * @param settings (object) - object containing keys that exist in Later's setup object
	 * @return object - Later's most recent settings
	 */
	_later.setup = function(settings) {
		if (typeof settings === 'object') {
			for (i in this.settings) {
				if (this.settings.hasOwnProperty(i) && typeof settings[i] !== 'undefined')
					this.settings[i] = settings[i];
			}
		}
		return this.settings;
	};

	/** 
	 * When called, it will call all the Later's sections' ctrl methods to unsave data from localStorage.
	 */
	_later.unsaveAll = function() {
		for (var i in sections) {
			if (sections.hasOwnProperty(i))
				sections[i].ctrl.unsave();
		}
	};

	/** 
	 * Finds all the <template /> elements and stores their innerHTML content into a variable named by templates' "name" attribute
	 */
	_later.loadTemplates = function() {
		var tmplts = document.getElementsByTagName('template');

		for (var i = 0, ii = tmplts.length; i < ii; i++) {
			var tmpl = tmplts[i];
			if (tmpl.getAttribute('name') != null) {
				templates[tmpl.getAttribute('name')] = nodeImport(tmpl);
			}
		}
	};

	/**
	 * Custom importNode() function that fixes a beautiful inconstinceny of IE that we all love from the bottom of our hearts.
	 * @param content (HTMLElement) - an element (of a template) we are about to import into later.
	 * @return clone (documentFragment) - a fragment containing nodes from inserted template.
	 */
	var nodeImport = function(content) {
		try {
			var clone = document.importNode(content.content, true);
		}
		catch(e) {
			var clone = document.createDocumentFragment();
			var childNodes = content.childNodes;

			for (var i = 0, ii = childNodes.length; i < ii; i++) {
				clone.appendChild(childNodes[i].cloneNode(true));
			}

			return clone.cloneNode(true);			
		}
		return clone;
	};

	/**
	 * A comparation method that decides whether two inserted variables are the same or not.
	 * Firstly a types are being matched. Then a basic comparison "==" for non-object variables is set.
	 * After that all the attributes of those object-like variables is inserted into array and those
	 * values are compared one-by-one.
	 *
	 * @param a (mixed) - a first variable's value that is being compared
	 * @param b (mixed) - a second variable's value to compare
	 * @return boolean  - statement whether compared arguments' values are equal or not.
	 */
	var compare = function(a, b) {
		if (typeof a != typeof b)
			return false;

		if (typeof a !== 'object') {
			return a === b;
		}

		var aL = [];
		var bL = [];
		for (var i in a) {
			if (a.hasOwnProperty(i)) {
				aL.push(a[i]);
			}
		}

		for (var i in b) {
			if (b.hasOwnProperty(i)) {
				bL.push(b[i]);
			}
		}

		if (aL.length != bL.length)
			return false;

		for (var i = 0, ii = aL.length; i < ii; i++) {
			if (aL[i] != bL[i])
				return false;
		}
		return true;
	};

	/**
	 * Returns a clone of inserted object.
	 * Object cannot contain any other rich data-formats as all of them will be replaced by primary data types (numbers, strings, objects).
	 * @param obj - inserted object to be cloned
	 * @return object - a cloned object. Changes made to an object returned by this method will not affect attributes of original object.
	 */
	var clone = function(obj) {
		if (obj === null || typeof obj !== 'object') {
			return obj;
		}

		var temp = obj.constructor(); // give temp the original obj's constructor
		for (var key in obj) {
			temp[key] = clone(obj[key]);
		}

		return temp;
	}

	/**
	 * When called within a argument, a corresponding saved template DOM tree will be returned. Or a whole `templates` object, when no argument is present
	 * @param name (string) - name of a template that is going to be returned. When not present a whole `templates` object is returned
	 * @return mixed (HTMLElement | object | false) - HTMLElement when an existing template's name is given. Object when... see above and `false` when invalid `name` is given.
	 */
	_later.template = function(name) {
		if (typeof name === 'undefined')
			return templates;
		
		if (typeof templates[name] !== 'undefined') {
			return templates[name].cloneNode(true);
		}

		return false;
	};

	/**
	 * Checkes whether a given variable does match a "pattern" defined by callback/listener variable name.
	 * I.e. Used when a template-generated variable "users.0.name" (e.c. variable) is compared to a pattern "+name" (i).
	 * @param i (string) - pattern to be compared by
	 * @param variable (string) - name of variable that is going to be matched
	 * @return boolean - true if a pattern matches, false otherwise
	 */
	var matchesPartOfList = function(i, variable) {
		var I = i.replace(/\+/, '');
		var regE = new RegExp('\\S+\\.' + I + '$');
		var regS = new RegExp('^' + I + '\\.\\S+');
		var regM = new RegExp('\\S+\\.' + I+ '\\.\\S+');
		var regF = new RegExp('^' + variable + '$');

		return (
			i.match(regF)
			|| i.match('GLOBAL')
			|| (i.match(/^\+/) && variable.match(regE))
			|| (i.match(/\+$/) && variable.match(regS))
			|| (i.match(/^\+.*\+$/) && variable.match(regM))
		);
	};

	/**
	 * A sub-closure to handle calls to/from the server. Contains methods used to obtain/send data to the given address in a asynchronous way.
	 */
	_later.ajaxer = (function() {
		var _a = {};
		var ajax = new XMLHttpRequest;
		var status = 0;

		/**
		 * Completes soon-to-be called URL to the browser-readable format. Transforms data to key-value pairs that will be sent to the server.
		 * @param url (string) - a given (preset) URL address that will be completed
		 * @param data (mixed) - an object whose inner variables will be transformed to URL's content
		 * @return string - a finalized URL address to be sent
		 */
		var completeUrl = function(url, data) {
			if (typeof data !== 'undefined' && data.length) {
				var urlized = urlize(data);
				var mergeBy = (url.indexOf('?') != -1) ? '&' : '?';
				url = url + mergeBy + urlized;
			}
			return url;
		};

		/**
		 * Inner function that creates a stringified version of a given object in a browser-readable standards.
		 * @param data (mixed) - an object whose variables will be transformed to the string
		 * @param parentKey mixed (int|string) - a key to track the way when a recursion happens
		 * @return string - an URL-friendly stringified version given object
		 */
		var urlize = function(data, parentKey) {
			var query = [];
			for (var i in data) {
				if (data.hasOwnProperty(i)) {
					var enci = encodeURIComponent(i);
					var key = (typeof parentKey !== 'undefined') ? parentKey + '[' + enci + ']' : enci;
					var push = (typeof data[i] === 'object') 
						? urlize(data[i], key)
						: key + '=' + encodeURIComponent(data[i]);
					query.push(push);
				}
			}
			return query.join('&');
		};

		/**
		 * Default requets method that starts AJAX request to server.
		 * Suitable to handle basic REST calls.
		 * @param method string - can contain only following uppercased string value: ['GET', 'PUT', 'POST', 'DELETE'].
		 * @param url stirng - a valid URL, the request is sent to. 
		 * @param data object - various primitive object with data to be sent onto server
		 * @param callback function - this function will be called when a successfull AJAX request is done. 
		 *							  It accepts only one argument - a responseText from AJAX call.
		 */
		var send = function(method, url, data, callback) {
			if (typeof callback === 'undefined' && typeof data === 'function')
				callback = data;

			if (typeof data === 'undefined')
				data = {};

			if (['GET', 'DELETE'].indexOf(method) > -1)
				url += '?' + urlize(data);

			ajax.open(method, url, true);
			ajax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			// ajax.setRequestHeader("Content-type","application/json");
			ajax.setRequestHeader("X-Requested-With","XMLHttpRequest");

			ajax.onreadystatechange = function() {
				if (ajax.readyState == 4 && ajax.status == 200) {
					status = ajax.status;
					if (typeof callback === 'function')
						return callback(ajax.responseText);
					return ajax.response;
				}
			};

			if (['GET', 'DELETE'].indexOf(method) > -1)
				ajax.send();
			else
				ajax.send(urlize(data));
		};

		/**
		 * Performs a GET, PUT, POST or DELETE call to the server to a given address with a specified data and after all of that runs a callback, when set.
		 * @param url (string) - an URL address the call will be performed to
		 * @param data (object) - actual data that will be sent to the specified address
		 * @param callback (function) - an abstract function to be called when an AJAX call will be finished
		 * @return - Returns whether a result of callback function when called or an AJAX result itself
		 */
		_a.get = function(url, data, callback) {
			send('GET', url, data, callback);
		};

		_a.put = function(url, data, callback) {
			send('PUT', url, data, callback);
		};

		_a.post = function(url, data, callback) {
			send('POST', url, data, callback);
		};

		_a.delete = function(url, data, callback) {
			send('DELETE', url, data, callback);
		};
		
		return _a;
	})();

	/**
	 * Returns an object of a registered section. When no argument is entered, method returns a list of all sections already registered.
	 * @param section (string) - name of a speciffic section to be returned. A section must be already registered!
	 * @return mixed (object|array) - whether a sectino object or list of section objects
	 */
	_later.section = function(sec) {
		if (typeof sec !== 'undefined') {
			return (typeof sections[sec] !== 'undefined') ? sections[sec] : {};
		}
		else
			return sections;

	};

	/**
	 * Checks, if an inserted element is a type of checkbox or radio
	 * @param element (HTMLElement) - inserted element
	 * @return boolean - true, if element is one of a kind, false otherwise
	 */
	var isElementCheckable = function(element) {
		return (['checkbox', 'radio', 'file'].indexOf(element.type) != -1 || element.tagName === 'SELECT');
	};

	var getFirstIterationParent = function(elem) {
			while (elem.parentElement != null && !elem.parentElement.hasAttribute('data-ljs-s')) {
				if (elem.hasAttribute('data-ljs-i'))
					return elem;
				elem = elem.parentElement;
			}

			return elem;
		}

	/**
	 * Returns element's value. Firstly finds every object with a name similar to inserted element, then returns all suitable values.
	 * If more equally named elements have returnable value, they will be stored as array. Otherwise a single value will be returned.
	 * @param element (HTMLElement) - inserted element, whose value we are about to get
	 * @return mixed - all obtainable values of selected element
	 */
	var getElementValue = function(name, element, giiLength) {
		var values = [];
		var marked = false;
		var elements;
		var section;

		if (giiLength)
			section = getFirstIterationParent(element);
		else if (typeof sections[name] !== 'undefined')
			section = sections[name].getDom();
		else {
			section = document.createElement('div');
			section.setAttribute('data-ljs-s', name);
		}

		if (element.tagName == 'SELECT') {
			if (typeof element.selectedOptions !== 'undefined')
				elements = element.selectedOptions;
			else {
				elements = [];
				var ii = element.options.length;
				for (var i = 0; i < ii; i++) {
					if (element.options[i].selected)
					elements.push(element.options[i]);
				}
			}
			marked = 'selected';
		}
		else {
			if (isElementCheckable(element)) {
				var nameToFind = '[name="' + element.getAttribute('name') + '"]';
				elements = section.querySelectorAll(nameToFind);
				marked = 'checked';
			}
			else
				elements = [element];
		}

		for (var i in elements) {
			// if (elements.hasOwnProperty(i) && elements[i].value != null && (!marked || elements[i][marked]))
			if (elements.hasOwnProperty(i) && elements[i].value != null) {
				if (!marked || elements[i][marked])
					values.push(elements[i].value);
				// else if (marked)
				// 	values.push(null);
			}
		}
		
		if (values.length > 1)
			return values;

		if (values.length)
			return values[0];
		return '';
	};

	/**
	 * A section closure. Creates an instance of section object with corresponding view/control methods
	 * @param name (string) - a name of section Must correspond to the existing DOM element's ID attribute
	 * @return - object instance of a newly registered section
	 */
	var section = function(name) {
		var _section = {};
		var callbackRecursion = [];
		var variableSendTimeouts = {};
		var savedDom = document.createElement('div');

		var initialized = false;
		var useLocalStorage = {
			section: false,
			varList: []
		};

		var sendPaths = {
			'get': '',
			'put': '',
			'post': '',
			'delete': ''
		};
		
		/**
		 * Returns a name of current section
		 */
		_section.name = function() {
			return name;
		};

		/**
		 * Returns/sets a section initialization statement.
		 * This statement is checked when a value is being stored into localStorage
		 * @param status (boolean) - when set, this value is being set as section's initialization status.
		 * @return (boolean) - status of section's initialization. True if section was finally initialized, false otherwise.
		 * @see isLocalStorageUsed
		 */
		_section.initialized = function(status) {
			if (typeof status === 'undefined')
				return initialized;

			if (typeof status === 'boolean')
				initialized = status;
			return initialized;
		};

		/**
		 * Returns current section's DOM
		 * If a section is not part of DOM, an initial pseudo-DOM will be created.
		 * @return - DOM element of current section
		 */
		_section.getDom = function() {
			var doms = document.querySelectorAll('[data-ljs-s="' + name + '"]');
			
			if (doms.length) {
				return doms[0];
			}
			else {
				if (savedDom.getAttribute('data-ljs-s') == null)
					savedDom.setAttribute('data-ljs-s', name);

				return savedDom;
			}
		};

		/**
		 * Based on a given element it returns the most probable action the EventListener should be waiting for.
		 * @param elem (HTMLElement) - an element whose action is going to be found
		 * @return string - an action name.
		 */
		var getAction = function(elem) {
			var clickables = ['DIV', 'SPAN', 'BUTTON', 'A'];

			if (clickables.indexOf(elem.tagName) != -1 || elem.type == 'submit')
				return 'click';
			else
				return (isElementCheckable(elem) || elem.type == 'range') ? 'change' : 'input';
		};

		/**
		 * Binds an event listener to the form element and after event is fired, section's ctrl.set() method to input's variable is run.
		 * Clickable elements get binding on click, elements with possibility to input data get "input" event trigger and "checkable" elements get "change" event trigger.
		 * @param name (string) - name of variable that will be set when an event is triggered
		 * @param elem (HTMLElement) - actual element that gets event binding
		 */
		var listenTo = function(name, elem, listener) {
			var action = getAction(elem);
			listener = listener || '';
			
			var gii = getIterationIndexes(elem);
			var fullvar = gii.length ? gii + '.' : '';
			var fullvarEnd = listener.length
				? listener
				: elem.getAttribute('name').replace(/\[.*?\]/, '');

			if (fullvarEnd.substr(0, 1) !== '@') {
				fullvar += fullvarEnd;
			}
			else {
				gii = [];
				fullvar = fullvarEnd.substr(1);
			}

			
			elem.addEventListener(action, function(event) {
				if (listener.length) {
					var listeners = _section.call.registered(fullvar);
					
					for (var i in listeners) {
						if (listeners.hasOwnProperty(i))
							listeners[i](fullvar.split('.'), event.target);
					}
				}
				else {
					var value = getElementValue(name, elem, gii.length);
					_section.ctrl.set(fullvar, value);
				}
			}, false);
				
		};

		/**
		 * Method traverses backwards to the main section element and collets every "data-ljs-i" attribute value.
		 * These collected values represent inheritance keys in an object and they are used to make a "path"
		 * to the variable inside this object. 
		 * Mostly usable while iterating arrays of objects. 
		 *
		 * @param HTMLElement elem - element whose parental keys are going to be found.
		 * @return string - dot-joined list of found keys.
		 */

		var getIterationIndexes = function(elem) {
			var es = [];
			while (elem.parentElement != null && !elem.parentElement.hasAttribute('data-ljs-s')) {
				if (elem.hasAttribute('data-ljs-i'))
					es.unshift(elem.getAttribute('data-ljs-i'));
				elem = elem.parentElement;
			}

			return es.join('.');
		}

		/**
		 * Method finds out if a given element is a direct child element of actual section.
		 * @param HTMLElement elem - element, whose alignment is tested
		 * @return boolean         - true, if given element is direct child element of acutal section, false otherwise.
		 */
		var isDirectSectionsElem = function(elem) {
			var es = 0;
			while (elem.parentElement != null && !elem.parentElement.hasAttribute('data-ljs-s')) {
				elem = elem.parentElement;
			}

			var par = elem.parentElement;
			return (par.hasAttribute('data-ljs-s') && par.getAttribute('data-ljs-s') == name);
		}

		/**
		 * Sets up usage of local storage per section. If result of this method is true, a localStorage may be used for data changes in current section's scope.
		 * 
		 * @param usage mixed (boolean | array) - if there is only this parameter and its type is boolean, TRUE/FALSE is set for using localStorage within whole section's scope.
		 * 										- if an usage is typeof array, a value of usage is automatically set to true (therefore adding variables is turned on) and list parameter is filled instead.
		 * @param list (array) - if a list is available, only variable available in this list will be stored in localStorage.
		 * @return useLocalStorage - a section's useLocalStorage variable with most current settings.
		 */
		_section.useLocalStorage = function(usage, list) {
			if (typeof usage === 'undefined' && typeof list === 'undefined') {
				return useLocalStorage;
			}

			list = list || [];
			
			if (typeof usage === 'boolean' && !list.length) {
				useLocalStorage.section = usage;
				useLocalStorage.varList = [];
				return;
			}

			if (typeof usage === 'object' && !list.length) {
				list = usage;
				usage = true;
			}
			
			if (typeof usage === 'boolean' && list.length) {
				var data = _section.ctrl.get();
				
				if (usage) {
					useLocalStorage.varList = useLocalStorage.varList.concat(list);
					useLocalStorage.varList = useLocalStorage.varList.reduce(function(a, b) {
						if (a.indexOf(b) < 0)
							a.push(b);
						return a;
					}, []);
				}
				else {
					for (var i in list) {
						var index = useLocalStorage.varList.indexOf(list[i]);
						if (index != -1)
							useLocalStorage.varList.splice(index, 1);
					}
				}
			}
			return;
		};

		/**
		 * Sets up default paths for section's AJAX requests.
		 * @param pathList object - list of URL paths. Must containt only object with following keys: get, put, post, delete.
		 * 							Each key must contain a string with url, the corresponding AJAX request will lead to.
		 * 							If no object is inserted, the current value of sendPaths variable is returned.
		 * @return sendPaths object - current sendPaths object value
		 */
		_section.sendPaths = function(pathList) {
			if (typeof pathList === 'undefined')
				return sendPaths;

			for (var i in pathList) {
				if (pathList.hasOwnProperty(i) && typeof sendPaths[i] !== 'undefined' && typeof pathList[i] === 'string') {
					sendPaths[i] = pathList[i];
				}
			}

			return sendPaths;
		};

		/**
		 * Simplified AJAX call for speciffic section. When sendPaths object is set properly, every AJAX request will be
		 * redirected to corresponding URL address with section's data list. 
		 * @param method string - only lowercased ['get', 'put', 'post', 'delete'] values are allowed.
		 * @param callback function - if defined, a callback function will be called when an AJAX request is finished successfully
		 */
		_section.send = function(method, callback) {
			var data = (method != 'get') ? _section.ctrl.get() : undefined;
			var url = sendPaths[method];
			var result = _later.ajaxer[method](url, data, callback);
		};

		/**
		 * Registers a callback that will be called whenever a refresh method will be called.
		 * To change the order of callbacks an existing list must be emptied and refilled.
		 * @param callname (string) - a name/identifier of newly registered callback
		 * @param calldata (mixed) - an abstract function that will be called
		 */
		_section.call = (function() {
			var _call = {};
			var listeners = {};

			/**
			 * Registers a new callback function into the list of callbacks
			 * @param callname (string) - name of newly registered callback function
			 * @param calldata (function) - a function to be called
			 * @param variable (string) - when this variable exists in a ctrl.data source, this callback will be triggered only when this variable changes
			 * @return boolean - true if a new callback function has been registered, false otherwise
			 */
			_call.register = function(variable, callname, calldata) {
				if (typeof calldata === 'undefined') {
					calldata = callname;
					callname = variable;
					variable = 'GLOBAL';
				}

				if (typeof callname === 'undefined') {
					console.error('Created listener function must be named.');
					return false;
				}

				if (typeof calldata === 'undefined' || typeof calldata !== 'function') {
					console.error('Registered "' + callname + '" is not a function.');
					return false;
				}

				// if (typeof variable !== 'undefined' && !_section.ctrl.isset(variable)) {
				// 	console.error('Trying to register the "' + callname + '" listener to a non-existing variable: "' + variable + '".');
				// 	return false;
				// }
				
				if (typeof listeners[variable] === 'undefined')
					listeners[variable] = {};
				listeners[variable][callname] = calldata;
				return true;
			};

			
			/**
			 * Unregisters a given callback name from callback list for this section. 
			 * @param callname (string) - a name of a callback function stored in a callback list
			 */
			_call.unregister = function(variable, callname) {
				if (typeof callname === 'undefined') {
					callname = variable;
					variable = 'GLOBAL';
				}
				
				if (typeof listeners[variable][callname] !== 'undefined') {
					delete(listeners[variable][callname]);
					return true;
				}
				else {
					console.error('Unregistered "' + callname + '" is not defined in "' + variable + '" listeners list.');
					return false;
				}
			};

			/**
			 * Unregisters all callbacks from callback list for this section.
			 */
			_call.unregisterAll = function() {
				listeners = {};
			};

			/**
			 * Returns list of registered callback functions for a specific variable or section globally
			 * @return (object) - associative array of registered callbacks; key = callback name, value = function itself
			 */
			_call.registered = function(variable) {
				var listenerList = {};
				variable = (typeof variable !== 'undefined') ? variable : 'GLOBAL';
				
				for (var i in listeners) {
					if (listeners.hasOwnProperty(i)) {
						if (matchesPartOfList(i, variable)) {
							for (var j in listeners[i]) {
								if (listeners[i].hasOwnProperty(j)) {
									listenerList[i + '_' + j] = listeners[i][j];
								}
							}
						}
					}
				}
				
				return listenerList;
			};

			/**
			 * Returns list of all registered listener functions
			 * @return (object) - associative array of registered listeners; key = callback name, value = function itself
			 */
			_call.registeredAll = function() {
				return listeners;
			};

			/** 
			 * Runs called callback function. Callback function must be set, of course
			 * @param variable (string) - name of registered element event
			 * @param callname (string) - name of called callback function
			 * @param argument (mixed)  - value of argument that will be entered into called function
			 * @param eventTarget (HTMLElement)  - element affected by triggering this event
			 * @return mixed - result of callback function
			 */
			_call.run = function(variable, callname, argument, eventTarget) {
				if (typeof callname === 'undefined') {
					callname = variable;
					variable = 'GLOBAL';
				}

				if (typeof listeners[variable][callname] === 'function')
					return listeners[variable][callname](argument, eventTarget);
				else {
					console.error('Called "' + callname + '" listener function is not a registered "' + variable + '" listener.');
					return false;
				}
			};

			/**
			 * Rebuilds a callback list based on an inserted list of callback names.
			 * When an unexisting callback name is in the list, it will not be registered as callback function.
			 * When any of already existing callback functions is not in this "rebuild list", it will be uncalled.
			 * Useful for i.e. rebuild order of called callbacks on section refresh
			 *
			 * @param listenerList (array) - an indexed list of callbacks that are going to be rebuild
			 */
			_call.rebuild = function(listenerList) {
				var newCallbacks = {};
				for (var i in listenerList) {
					if (typeof callbacks[listenerList[i]] === 'function')
						newCallbacks[listenerList[i]] = callbacks[listenerList[i]];
				}
				callbacks = newCallbacks.copy();
			}

			return _call;
		})();

		/**
		 * A section's control sub-closure. Contains methods used to achieve data changes to the section (i.e. to update values of related form elements).
		 */
		_section.ctrl = (function() {
			var _ctrl = {};
			var path = '';
			var data = {};

			/**
			 * A recursive method that sets object within object where the parent's key is a part of given array value.
			 * @param parentObject (object) - firstly it is a whole data object of this sub-closure. In any other recursive call will be replaced by local object.
			 * @param list (array) - an indexed array of variables that represent the keys of objects the every newly created object is indexed in its parent.
			 * 					   - i.e.: array['hello', 'world'] will lead to creation of an object: {hello {world: null}}
			 * @param value (mixed) - when a whole list is traversed, the final value will be associated to the last list's value
			 * @return object - a local object will be returned - with all its sub-objects created in every recursive call of method.
			 */
			var nestVars = function(parentObject, list, value, remove) {
				var key = list.shift();
				var obj = (typeof parentObject[key] !== 'undefined') ? parentObject[key] : {};

				// value = (typeof value === 'string') ? escapeHtml(value) : value;

				if (remove && !list.length) {
					if (parentObject instanceof Array)
						parentObject.splice(key, 1);
					else
						delete(parentObject[key]);
				}
				else
					parentObject[key] = (list.length) ? nestVars(obj, list, value, remove) : value;
				return parentObject;
			};

			/**
			 * A simple escape method that replaces special html characters into html-friendly strings.
			 * Strings must be escaped because when any new HTML element tags would be inserted via variable into DOM, whole parser would crash because of unequality of parent template and actual DOM.
			 * @param text (string) - a string whose content will be replaced
			 * @return string - a replaced result
			 */
			var escapeHtml = function(text) {
				var map = {
					'&': '&amp;',
					'<': '&lt;',
					'>': '&gt;',
					'"': '&quot;',
					"'": '&#039;'
				};

				return text.replace(/[&<>"']/g, function(m) { return map[m]; });
			};

			/**
			 * Loads an actual object data from a server based on a set URL address
			 * @param callback (function) - if a function is put as argument, it will be triggered right after load itself is done.
			 */
			_ctrl.load = function(callback) {
				return _later.ajaxer.get(path, {}, function(result) {
					var loadedData = JSON.parse(result);
					for (var i in loadedData) {
						if (loadedData.hasOwnProperty(i)) {
							_ctrl.set(i, loadedData[i], false);
						}
					}
					_section.view.refresh();

					if (typeof callback === 'function') {
						callback();
					}
				});
			}

			/**
			 * Saves the actual section's ctrl data to the localStorage
			 * return mixed (true|null) - null if no local storage is present, else true
			 */
			_ctrl.save = function() {
				if (typeof window.localStorage !== 'undefined') {
					if (typeof window.localStorage['ljs_' + name] === 'undefined')
						window.localStorage['ljs_' + name] = '';
					var dataToSave = {};

					if (!useLocalStorage.varList.length)
						dataToSave = data;
					else {
						for (var i in data) {
							if (data.hasOwnProperty(i) && useLocalStorage.varList.indexOf(i) != -1) {
								dataToSave[i] = data[i];
							}
						}
					}
					window.localStorage['ljs_' + name] = JSON.stringify(dataToSave);
					return true;
				}
				return null;
			};

			/**
			 * Restores all data from local storage and saves it to the section's ctrl data object
			 * @return mixed (true|null) - null if no local storage is present, else true
			 */
			_ctrl.restore = function(init) {
				if (isLocalStorageUsed() 
					&& typeof window.localStorage['ljs_' + name] !== 'undefined'
				) {
					var savedData = JSON.parse(window.localStorage['ljs_' + name]);
					for (var i in savedData) {
						if (savedData.hasOwnProperty(i))
							_ctrl.set(i, savedData[i]);
					}
					return true;
				}
				return null;
			};

			/**
			 * Erases all the data that belong to this section from local storage
			 * @return mixed (true|null) - null if no local storage is present, else result of delete() function
			 */
			_ctrl.unsave = function() {
				if (typeof window.localStorage !== 'undefined') {
					if (typeof window.localStorage['ljs_' + name] !== 'undefined')
						return delete(window.localStorage['ljs_' + name]);
				}
				return null;
			};

			/**
			 * Returns a boolean statement whether a local storage is used for keeping value of given variable. 
			 * If no variable is set, a statement is returned for whether saving whole section's data object list.
			 * @param variable (string) - a name of variable or dot-separated-path to the subvariable that is about to be testet.
			 * @return boolean - true if a local storage saving for this variable is used, false otherwise.
			 */
			var isLocalStorageUsed = function(variable) {
				variable = (typeof variable !== 'undefined') ? variable : null;

				return (typeof window.localStorage !== 'undefined' 
					&& initialized
					&& (
						_later.settings.useLocalStorage 
						|| useLocalStorage.section 
						|| (variable != null && useLocalStorage.varList.indexOf(variable) != -1)
					)
				);
			};

			/**
			 * Gets an actual value of a variable of section's data object.
			 * @param variable (mixed [string|array]) - name of a variable whose value we want to get.
			 * 										 - if none is given, a whole data object will be returned.
			 * 										 - if a string is given and does not contain any dot, a search for corresponding variable will be processed and its value returned.
			 * 										 - otherwise if a string contains dot, it will be parsed and recursive call of .get() method will be called until a searched variable of given sub-object will be returned.
			 * @param root (object) - a source data object where we are going to look for given variable
			 * @return mixed - look to the first line
			 */
			_ctrl.get = function(variable, root) {
				if (typeof variable === 'undefined')
					return clone(data);

				if (variable == null) {
					console.warn('Cannot get value of `null`.');
					return null;
				}

				root = (typeof root !== 'undefined') ? root : data;
				if (typeof variable === 'string')
					variable = variable.split('.');
				var key = variable.shift();

				if (variable.length)
					return _ctrl.get(variable, root[key]);
				else
					return (typeof root[key] === 'object') ? clone(root[key]) : root[key];
			};

			/**
			 * Sets up section's data object
			 * @param value (mixed string|object) - name of a variable to be set or a whole object that will be linked to section's data
			 * @param subvalue (mixed) - a various data that will be stored to "value" variable of a section's data object
			 * @param refresh (boolean) - if true, any callbacks registered to this value change event will be run
			 */
			_ctrl.set = function(variable, value, refresh) {
				refresh = (typeof refresh === 'boolean') ? refresh : null;

				if (typeof variable === 'object') {
					refresh = (typeof value === 'boolean') ? value : refresh;
					
					for (var i in variable) {
						if (variable.hasOwnProperty(i)) {
							_ctrl.set(i, variable[i], refresh);
						}
					}
				}
				
				else {
					var orig = _section.ctrl.get(variable);

					if (!compare(orig, value)) {
						var splitted = variable.split('.');
						data = nestVars(data, splitted, value);
						refresh = (refresh != null) ? refresh : true;
					}

					if (typeof _section.view !== 'undefined' && refresh) {
						_section.view.refresh(variable);
					}

					if (isLocalStorageUsed(variable)) {
						_ctrl.save();
					}
				}
			}

			/**
			 * Removes defined variable. When want to remove any subvariable of an object, enter path to this variable separeted by dots.
			 * @param variable (object) - variable to be deleted from a section's ctrl data object. Example: "delete-me", "del.my.subvar".
			 */
			_ctrl.del = function(variable) {
				if (typeof variable === 'undefined')
					data = {};
				else
					nestVars(data, variable.split('.'), null, true);
			};

			/**
			 * Returns a statement whether a given variable is set inside section's data object or not
			 * @param variable (mixed[string|array]) - name of a variable whose existence we are about to find. 
			 * 										   If a variable is written with dots (i.e.: character.name), a recursive call will be run to find corresponding value of object variable.
			 * @param root (object) - a source of data where we will look for variable's value
			 * @return boolean - true if a given variable exists, false otherwise
			 */
			_ctrl.isset = function(variable, root) {
				root = (typeof root !== 'undefined') ? root : data;

				if (typeof variable === 'string')
					var variable = variable.split('.');
				var key = variable.shift();
				
				if (typeof root[key] !== 'undefined')
					return (variable.length) ? _ctrl.isset(variable, root[key]) : true;
				else
					return false;
			}

			return _ctrl;
		}(_section.getDom));

		/**
		 * A section's view sub-closure. Contains methods used to achieve visual changes to the DOM element paired with this section.
		 */
		_section.view = (function() {
			var formElements = ['INPUT', 'SELECT', 'TEXTAREA', 'BUTTON'];
			var _view = {};
			var path = '';
			var template = '';
			
			/**
			 * Stores the actual inner HTML content of a paired DOM element to the internal template variable.
			 * I a section is created from a scratch and does not exist in DOM yet, a plain div element is created.
			 */
			var init = function() {
				template =_section.getDom().cloneNode(true);
				_view.refresh(null);

				var templates = document.getElementsByTagName('template');
					for (var i = 0, ii = templates.length; i < ii; i++)
						templates[i].style.display = 'none';
			};

			/**
			 * Sets up the corresponding CSS values to the current section
			 * @param dom (HTMLElement) - current DOM
			 * @param variable (string) - existing style.variable
			 * @value value (string) - desirable CSS value of style variable
			 * @return boolean - True if CSS setting was succesfull, false otherwise
			 */
			var processCss = function(dom, variable, value) {
				if (typeof dom.style[variable] === 'undefined') {
					console.error('Specified "style" variable (' + variable + ') does not exist.');
					return false;
				}
				dom.style[variable] = value;
				return true;
			};

			/**
			 * Simple helper method to determine whether an inserted element is part of form elements group (aka. button, input, select, textarea)
			 * @param elem (HTMLElement) - tested element
			 * @return boolean - true if the element is part of form elements, false otherwise
			 */
			var isFormElement = function(elem) {
				return (formElements.indexOf(elem.tagName) != -1);
			};

			/**
			 * Puts an internally kept HTML content to the paired document's element. Used for section-reset.
			 * Eliminates all bindings to the current elements inside current section!
			 */
			_view.reset = function() {
				var element =_section.getDom();
				element = template;
			};

			/**
			 * Gets an internally kept template HTML content
			 * @return HTMLElement
			 */
			_view.getTemplate = function() {
				return template.cloneNode(true);
			};

			/**
			 * Updates section's template variable with a given content. When (boolean) doRender is true, this content will be also put to actual section. 
			 * @param content (HTML parsed string) - string that contains full template DOM
			 * @doRender (boolean [false]) - when true, inserted template value will be pushed to the browser to be shown
			 */
			_view.setTemplate = function(content, doRefresh) {
				while (template.firstChild)
					template.removeChild(template.firstChild);

				template.appendChild(content);

				doRefresh = doRefresh || false;

				if (doRefresh)
					_view.refresh();
			};

			/**
			 * Updates only section's content, so the section itself will not be touched.
			 * Ideal for inserting results of AJAX calls etc.
			 * @param content (mixed) - a data that will replace the section's innerHTML content
			 */
			_view.setContent = function(content, doRefresh) {
				var dom =_section.getDom();
				while (dom.firstChild)
					dom.removeChild(dom.firstChild);

				dom.appendChild(content.cloneNode(true));

				doRefresh = doRefresh || false;

				if (doRefresh)
					_view.refresh();
			};

			/**
			 * A classic getter for section's content. Content is parsed into documentFragment and sent out.
			 * @return documentFragment - a fragment that contains section's content.
			 */
			_view.getContent = function() {
				var df = document.createDocumentFragment();
				var childNodes =_section.getDom().childNodes;

				for (var i = 0, ii = childNodes.length; i < ii; i++) {
					df.appendChild(childNodes[i].cloneNode(true));
				}

				return df.cloneNode(true);
			};

			/**
			 * Finds an element with a "name" attribute or a {{name}} snippet and changes its stored/inserted value.
			 * @param updatedVar (string | null) - a name of a variable that was already updated. Can be string or null when no specific variable was given.
			 */
			_view.refresh = function(updatedVar) {
				updatedVar = updatedVar || null;
				
				if (_view.isHidden())
					return true;

				var dom =_section.getDom();
				var temp = template.cloneNode(true);

				cycleRefresh(dom, temp, updatedVar);
				tmpltRefresh(dom, temp);
				
				parseFormElements(dom, updatedVar);
				parseLjsListeners(dom);

				refreshFormElements(dom);

				if (updatedVar != null && callbackRecursion.indexOf(updatedVar) == -1) {
					callbackRecursion.push(updatedVar);

					var callbacks = _section.call.registered();

					for (var i in callbacks) {
						if (callbacks.hasOwnProperty(i))
							callbacks[i](updatedVar.split('.'));
					}

					if (typeof updatedVar === 'string') {
						var callbacks = _section.call.registered(updatedVar);
						for (var i in callbacks) {
							if (callbacks.hasOwnProperty(i))
								callbacks[i](updatedVar.split('.'));
						}
					}
				}
				callbackRecursion.splice(callbackRecursion.indexOf(updatedVar), 1);
			};

			/**
			 * This method runs through every section's dom element and when this element does not contain
			 * any other child element nodes, a "replaceCycleVars" method is ran.
			 *
			 * @param dom (HTMLElement) - a section's realtime node
			 * @param node (HTMLElement) - a section's template node
			 * @param updatedVar (mixed) - a name of updated variable. Default is null
			 */
			var cycleRefresh = function(dom, node, updatedVar) {
				if (dom.getAttribute('data-ljs-s') != null && dom.getAttribute('data-ljs-s') !== name)
					return;

				var ii = node.children.length;
				if (ii) {
					for (var i = 0; i < ii; i++) {
						cycleRefresh(dom.children[i], node.children[i], updatedVar);
					}
				}
				else {
					replaceCycleVars(dom, node, updatedVar);
					
					var childrenAfter = node.children.length;
					if (ii < childrenAfter) {
						ii = childrenAfter;
						for (var i = 0; i < ii; i++) {
							cycleRefresh(dom.children[i], node.children[i], updatedVar);
						}
					}
				}
			};

			/**
			 * Initializing method that cascades whole template DOM and whether calls itself or replaces all strings "{{anything}}" with actual data.
			 * @param dom (HTMLElement) - actual DOM element (or whole DOM)
			 * @param node (HTMLElement) - corresponding "shadow DOM" - initial DOM state stored in template variable
			 * @param updatedVar (string | null) - a name of a variable that was already updated. Can be string or null when no specific variable was given.
			 */
			var tmpltRefresh = function(dom, node) {
				if (dom.getAttribute('data-ljs-s') != null && dom.getAttribute('data-ljs-s') !== name)
					return;


				var ii = node.children.length;
				if (ii) {
					for (var i = 0; i < ii; i++) {
						tmpltRefresh(dom.children[i], node.children[i]);
					}
				}
				else
					replaceSnippetVars(dom, node, false);
				replaceSnippetVars(dom, node, true);
			};

			/**
			 * Method simillar to "replaceSnippetVars", but job of this one is to change speciffic snippets.
			 * Those snippets have this form: "{{template_name in list_of_values}}".
			 * This snippet replaces itself with a noted template's content so many times, how many elements has iterated list of values.
			 * For each iteration are all the values of template renamed, as they get the prefix comsposed from list variable name and current iteration key.
			 * Thus, changed template list is saved into array and returned to the section, where it is saved for later use.
			 * 
			 * @param dom HTMLElement - current DOM
			 * @param node HTMLElement - section's saved template node
			 * @param updatedVar string - name of variable that is currently being updated. WHen updated variable is matched, its content is updated too,
			 * 							  otherwise a latest version of corresponding DOM is assigned.
			 */
			var replaceCycleVars = function(dom, node, updatedVar) {
				var nodeChanged = false;
				var reg = new RegExp('\\{\\{(\\S+)\\s+in\\s+(\\S+)\\}\\}', 'g');
				var ljsa = node.textContent.match(reg);
				if (ljsa != null) {
					var result = cycleVar(ljsa);
					
					for (var i = 0, ii = dom.childNodes.length; i < ii; i++) {
						for (var j = 0, jj = ljsa.length; j < jj; j++) {
							if (dom.childNodes[i].textContent.match(ljsa[j]))
								dom.childNodes[i].textContent = '';
						}
					}


					nodeMerger(dom, node, result);
				}

				return nodeChanged;
			};

			/**
			 * Recursively calls itself like in "tmpltRefresh" method case.
			 * For every basic element node (with no additional child element nodes) compares
			 * whether an attributes of "cycleVar" method result elements are similar to 
			 * live dom or node elements'. If not, those values are set. The same applies to 
			 * dom's / node's textContent. 
			 * If dom's/node's child node does not exist at all, it is cloned from result.
			 *
			 * @param dom HTMLElement - current DOM
			 * @param node HTMLElement - section's saved template node
			 * @param result HTMLElement - result of cycleVar method - a dummy element with content of cycle replacement snippet
			 */
			var nodeMerger = function(dom, node, result) {
				var ii = result.children.length;
				var xx = dom.children.length;
				var yy = node.children.length;

				if (ii && ii == xx && ii == yy) {
					for (var i = 0; i < ii; i++) {
						nodeMerger(dom.children[i], node.children[i], result.children[i]);
					}
				}
				else if (!ii) {
					for (var i = 0; i < result.attributes; i++) {
						dom.setAttribute(result.attributes[i].nodeName, result.attributes[i].value);
						node.setAttribute(result.attributes[i].nodeName, result.attributes[i].value);
					}
					dom.textContent = result.textContent;
					node.textContent = result.textContent;
				}
				else {
					while (ii < dom.children.length) {
						dom.removeChild(dom.lastElementChild);
					}
					
					while (ii < dom.children.length) {
						node.removeChild(node.lastElementChild);
					}
					
					if (ii != xx) {
						for (var i = 0; i < ii; i++) {
							if (typeof dom.children[i] === 'undefined')
								dom.appendChild(result.children[i].cloneNode(true));
						}
					}

					if (ii != yy) {
						for (var i = 0; i < ii; i++) {
							if (typeof node.children[i] === 'undefined')
								node.appendChild(result.children[i].cloneNode(true));
						}
					}
				}
			}

			/** 
			 * If a snippet variable matches pattern "{{template_name in variable_list}}", a cycleVar is called.
			 * Method creates temporary section whose main template is set by matched template name and data list is
			 * set to match array variable's values. When a section is refreshed, its HTML content is extracted and put into variable.
			 * That variable is later sent as pattern's replacement value.
			 * 
			 * @param match (array) - a regular expression result that looks like: [{{temp in varlist}}, temp, varlist].
			 * @return string - innerHTML of a temporary element filled with a content of a section made in cycles.
			 */
			var cycleVar = function(matches) {
				var temp = _later.register('_temp');
				var reg = new RegExp('\\{\\{(\\S+)\\s+in\\s+(\\S+)\\}\\}');

				var newNodes = document.createElement('div');

				for (var i = 0, ii = matches.length; i < ii; i++) {
					var match = matches[i].match(reg);
					var giiBase = match[2].split('.');
					var tmplt = _later.template(match[1]);
					var value = _section.ctrl.get(match[2]);

					if (!tmplt || typeof value === 'undefined')
						return newNodes;

					var giiIdx = (giiBase.length > 2) ? giiBase.slice(-1)[0] : giiBase.join('.');
					
					for (var j = 0, jj = value.length; j < jj; j++) {
						temp.view.setTemplate(_later.template(match[1]));

						var templ = temp.view.getTemplate();

						cycleVarReplacer(templ, j, match[2]);

						templ.firstElementChild.setAttribute('data-ljs-i', giiIdx + '.' + j);
						newNodes.appendChild(templ.firstElementChild);
					}
				}
				
				_later.free('_temp');
				return newNodes;
			};

			/**
			 * Method runs through given node children nodes and when they exist no more,
			 * every child element node is being tested for existence of "{{}}" snippets, listeners and "name" attribute.
			 * Every matched attribute value is then changed to match current replacement and index values.
			 * The same exchange is set to every element's textContent "{{}}" snippet.
			 *
			 * @param templ HTMLElement  - element whose values and text content is being tested/replaced.
			 * @param index INT          - index of iteration for which this current templ element is being processed.
			 * @param replacement STRING - a section list's name, which is currently being iterated
			 * @see cycleVar method
			 */
			var cycleVarReplacer = function(templ, index, replacement) {
				var ii = templ.children.length;
				if (ii) {
					for (var i = 0; i < ii; i++) {
						cycleVarReplacer(templ.children[i], index, replacement);
					}
				}
				else {
					templ.textContent = templ.textContent
						.replace(/\{\{(\S*?)\}\}/g, '{{' + [replacement, index, '$1'].join('.') + '}}')
						.replace(/\{\{.*?\.@(\S*?)\}\}/g, '{{$1}}')
						.replace(/(\{\{.*?in\s)(\S*?)\}\}/g, '$1' + [replacement, index, '$2'].join('.') + '}}')
						.replace(/(\{\{.*?in\s)\S*?\.@(\S*?)\}\}/g, '$1$2}}');
				}

				var reg = new RegExp('\\{\\{(\\S+)\\}\\}', 'g');
				var rx = new RegExp('\\{\\{(\\S+)\\}\\}');
				for (var j = 0, jj = templ.attributes.length; j < jj; j++) {
					var match = templ.attributes[j].value.match(reg);
					if (match != null) {
						for (var x = 0, xx = match.length; x < xx; x++) {
							var mtch = match[x].match(rx);
							if (mtch != null) {
								templ.attributes[j].value = templ.attributes[j].value.replace(mtch[0], '{{' + replacement + '.' + index + '.' + mtch[1] + '}}');
							}
						}
					}
				}
			};

			/**
			 * Method replaces current node's snippets into real section's data variable values.
			 *
			 * @param nodeData HTMLElement - current sections DOM element whose content is being replaced.
			 * @param defReg string        - a regular expression that is being used for variable matches
			 * @param value                - a default value that is being set into matched section variables
			 * @param updatedVar           - name of section's data variable that is being set. Default = null
			 * @return array[HTMLElement, boolean] - First element of this array is changed DOM element. 
			 * 										 Second is a boolean statemenet whether an element was changed or not.
			 */
			var snippetReplacer = function(nodeData, defReg) {
				var data = nodeData.match(defReg);
				if (data != null) {
					var keyReg = new RegExp('(\\d+)\\.\\#key');
					var result = nodeData;
					
					for (var i = 0, ii = data.length; i < ii; i++) {
						var dataClean = data[i].replace(/[\{\}]/g, '');
						dataClean = dataClean.replace(/.*@/, '');
						dataClean = dataClean.replace(/\.\#value.*/, '');
						
						var keymatch = dataClean.match(keyReg);
						var dataValue = (keymatch == null) ? _section.ctrl.get(dataClean) : keymatch[1];
						dataValue = (typeof dataValue !== 'undefined') ? dataValue : '';
						result = result.replace(data[i], dataValue);
					}
				}

				return result;
			}

			/**
			 * Private method that replaces all string occurences like "{{anything}}" inside outerHTML content of any element.
			 * Every matched string will be parsed and replaced by a corresponding section ctrl data value.
			 * When matched string contains name that is not already preset in the section ctrl datase, 
			 * this variable will be created with an empty string as value.
			 *
			 * @param node (HTMLElement) - node of inner template DOM that contains initial HTML template.
			 * @param dom (HTMLElement) - node of the latest website DOM that is being compared and updated
			 * @param updatedVar (string | null) - a name of a variable that was already updated. Can be string or null when no specific variable was given.
			 */
			var replaceSnippetVars = function(dom, node, onlyAttributes) {
				onlyAttributes = typeof onlyAttributes === 'boolean' ? onlyAttributes : false;
				var reg = new RegExp('\\{\\{.+?\\}\\}', 'g');
				
				if (onlyAttributes) {
					if (!node.hasAttribute('data-ljs-e') && !node.hasAttribute('ljs-e')) {
						var attribute, result, newAttrName, attr, attrsToRemove = [];
						
						for (var i = 0, ii = node.attributes.length; i < ii; i++) {
							attribute = node.attributes.item(i);

							// Check for LaterJS snippets inside attribute values.
							var ljsv = attribute.value.match(reg);
							result = (ljsv != null) ? snippetReplacer(attribute.value, reg) : dom.attributes.getNamedItem(attribute.name).value;
							
							if (attribute.name.match(/^ljs-/)) {
								newAttrName = attribute.name.replace(/^ljs-/, '');
								attrsToRemove.push(attribute.name);
							}
							else
								newAttrName = attribute.name;

							dom.setAttribute(newAttrName, result);
						}

						for (var i = 0, ii = attrsToRemove.length; i < ii; i++)
							dom.removeAttribute(attrsToRemove[i]);
					}
				}
				else {
					var ljst = node.textContent.match(reg);
					if (ljst != null) {
						var result = snippetReplacer(node.textContent, reg);
						if (dom.textContent != result) 
							dom.textContent = result;
					}
				}
			};

			/**
			 * Replaces and sets up values for every form element that occurs in section. 
			 * Creates variables in section's ctrl dataset based on every form element's name.
			 * If more elements with the same name is present (i.e. checkboxes, radios or just name="name[]" - array), an array of values will be created.
			 * @param dom (HTMLElement) - actual DOM that is going to be parsed
			 * @n (HTMLElement) - node of form element that should be parsed. If not present, whole DOM will be parsed
			 * @v (mixed) - value of corresponding "n" node of the form element. If not present, an empty value will be inserted
			 */
			var refreshFormElements = function(dom) {
				var data = _section.ctrl.get();
				var nodes = dom.querySelectorAll('[name]')
				
				if (!nodes.length)
					return;

				for (var i = 0, ii = nodes.length; i < ii; i++) {
					if (!(nodes[i] instanceof HTMLElement) || nodes[i].tagName == 'TEMPLATE' || !isDirectSectionsElem(nodes[i]))
						continue;
					
					var n = nodes[i].name.replace(/\[.*/, '');
					var gii = getIterationIndexes(nodes[i]);

					if (n.substr(0, 1) !== '@')
						var variable = gii.length ? gii + '.' + n : n;
					else
						var variable = n.substr(1);

					var v = _section.ctrl.get(variable);
					
					if (typeof v === 'undefined')
						continue;

					v = (v instanceof Array) ? v : [v];
					for (var j = 0, jj = v.length; j < jj; j++) {
						v[j] = v[j].toString();
					}

					if (nodes[i].tagName === 'SELECT') {
						var options = nodes[i].getElementsByTagName('option');
						
						for (var j = 0, jj = options.length; j < jj; j++) {
							options[j].selected = false;
							
							if (v.indexOf(options[j].value) != -1)
								options[j].selected = true;
						}
					}

					else if (isElementCheckable(nodes[i])) {
						nodes[i].checked = false;
						if (v.indexOf(nodes[i].value) != -1)
							nodes[i].checked = true;
					}
					else {
						if (nodes[i].value !== v[0])
							nodes[i].value = v[0];
					}
				}
			};

			/**
			 * Searches a whole section for any element with a `name` attribute. 
			 * When a variable with this name attribute's value exists and is updated, a value from corresponding sections' data object is given, otherwise element's value is given.
			 * @param dom (HTMLElement) - actual DOM or its subtree
			 * @param updatedVar (string | null) - a name of a variable that was already updated. Can be string or null when no specific variable was given.
			 */
			var parseFormElements = function(dom, updatedVar) {
				var formElements = dom.querySelectorAll('[name]');
				for (var i in formElements) {
					if (formElements[i] instanceof HTMLElement && isDirectSectionsElem(formElements[i])) {
						var gii = getIterationIndexes(formElements[i]);
						var variable = formElements[i].getAttribute('name').replace(/\[.*/, '');

						if (variable.substr(0, 1) !== '@')
							variable = gii.length ? gii + '.' + variable : variable;
						else {
							gii = [];
							variable = variable.substr(1);
						}

						var updatedVarsVal = (updatedVar == variable) ? _section.ctrl.get(updatedVar) : _section.ctrl.get(variable);
						var value = (updatedVarsVal != null) ? updatedVarsVal : getElementValue(name, formElements[i], gii.length);
						
						if (!formElements[i].hasAttribute('data-ljs-e')) {
							formElements[i].setAttribute('data-ljs-e', true);
							listenTo(name, formElements[i]);
						}
						
						if (value != updatedVarsVal) {
							_section.ctrl.set(variable, value, false);
						}
					}
				}
			};

			/**
			 * Searches a whole section for any element with a `data-ljs-l` attribute and registers it to a list of listeners.
			 * @param dom (HTMLElement) - actual DOM or its subtree
			 */
			var parseLjsListeners = function(dom) {
				var listeners = dom.querySelectorAll('[data-ljs-l]');
				
				for (var i in listeners) {
					if (listeners[i] instanceof HTMLElement && isDirectSectionsElem(listeners[i])) {
						var listener = null;
						if (typeof listeners[i].dataset !== 'undefined' && typeof listeners[i].dataset['ljsL'] !== 'undefined') {
							listener = listeners[i].dataset['ljsL'];
						}
						
						else if (listeners[i].getAttribute('data-ljs-l') != null) {
							listener = listeners[i].getAttribute('data-ljs-l');
						}

						if (listener != null && !listeners[i].hasAttribute('data-ljs-e')) {
							listeners[i].setAttribute('data-ljs-e', true);
							listenTo(name, listeners[i], listener);
						}
					}
				}
			};

			/**
			 * Shows current section if hidden
			 * @param displayValue (string) - desired display value of the section. "initial" as default.
			 */
			_view.show = function(displayValue) {
				var dom =_section.getDom();
				displayValue = displayValue || 'inline-block';
				
				if (dom.style.display != displayValue) {
					dom.style.display = displayValue;
				}
			};

			/**
			 * Hides current section if shown
			 */
			_view.hide = function() {
				var dom =_section.getDom();
				if (dom.style.display != 'none') {
					dom.style.display = 'none';
				}
			};

			/**
			 * Checks whether a current section is hidden
			 * @return boolean - true if section is hidden (style.display = 'none'), false otherwise.
			 */
			_view.isHidden = function() {
				var dom =_section.getDom();
				return (dom.style.display == 'none');
			};

			/**
			 * Check whether a current section is hidden via .isHidden() method. Then proceeds corresponding method to show/hide it.
			 */
			_view.toggle = function() {
				if (_view.isHidden())
					_view.show();
				else
					_view.hide();
			};

			/**
			 * Sets inline CSS styles of a current section
			 * @param variable (mixed string|object) - if a variable is string, is compared to the existing HTMLElement.style object variable 
			 * list and when in list, is set with a "value" argument. If a variable is an object, its key/value pairs are used in cycle as described.
			 * @param value (mixed number|string) - value of a corresponding CSS style variable
			 */
			_view.css = function(variable, value) {
				var dom =_section.getDom();
				if (typeof value !== 'undefined')
					processCss(dom, variable, value)
				else if (typeof variable === 'object') {
					for (i in variable) {
						processCss(dom, i, variable[i]);
					}
				}
			}

			_view.addClass = function(className) {
				var dom = _section.getDom();
				if (!dom.classList.contains(className))
					dom.classList.add(className);
			}

			_view.removeClass = function(className) {
				var dom = _section.getDom();
				if (dom.classList.contains(className))
					dom.classList.remove(className);
			}

			_view.toggleClass = function(className) {
				var dom = _section.getDom();
				dom.classList.toggle(className);
			}

			_view.hasClass = function(className) {
				var dom = _section.getDom();
				return dom.classList.contains(className);
			}

			init();

			return _view;
		}(_section.getDom));
		
		_section.view.getIterationIndexes = getIterationIndexes;

		return _section;
	}

	_later.setup(externalSettings);
	if (_later.settings.autostart)
		_later.init();

	return _later;
});