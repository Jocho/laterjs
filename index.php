<?php
$method = $_SERVER["REQUEST_METHOD"];
if (in_array($method, array('PUT', 'POST', 'DELETE'))) {
	if ($method == 'PUT')
		parse_str(file_get_contents('php://input'), $_POST);
	
	if (isset($_POST["ajax_response"])) {
		$r = array(
			'Hello,',
			'You have entered a following text:',
			'"' . $_POST["text"] . '"',
			"Nice one!"
		);
		echo json_encode(implode(' ', $r));
		die();
	}
}


function fs($fname) {
	return round(filesize($fname) / 1000, 0);
}
$size1 = fs('later.js');
$size2 = fs('later.min.js');

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="robots" content="all" />
		<meta name="generator" content="sublime text 3" />
		<meta name="author" content="Marek J. Kolcun" />
		<meta name="keywords" content="lightweight, JS, framework, later.js" />
		<meta name="description" content="Later.js is lightweight JS framework!" />
		<meta name="viewport" content="width=device-width, user-scalable=no" />
		<link rel="stylesheet" type="text/css" href="style.css" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="later.js"></script>
		<script type="text/javascript" src="examples.js" defer="defer"></script>
		<link rel="shortcut icon" href="favicon.ico" />
		
		<title>Later.js</title>
	</head>
	<body>
		<div class="row">
			<div class="column8 center">
				<div class="row">
					<h1>LaterJS</h1>
					<blockquote class="center tac">LaterJS is a tool that does just what it promises!</blockquote>
					<div class="column10" data-ljs-s="intro">
						<p>A self-education oriented tool that shows my capabilities.</p>
						<p>This tool is a lightweight JS framework with full two-way data binding and basic AJAX methods to communicate with server.</p>
						<p>Fully documented version can be downloaded here: <a href="later.js" download class="right">later.js v. {{version}} [<?= $size1; ?>kB]</a></p>
						<p>A minified version is available here: <a href="later.min.js" download class="right">later.min.js v. {{version}} [<?= $size2; ?>kB]</a></p>
						<p>Project can be found on a Bit Bucket's Git: <a href="http://bitbucket.org/Jocho/laterjs" target="_blank" class="right">Bit Bucket - a home of LaterJS</a></p>
						<br><br>
						<p class="tar"> created by: Marek. J. Kolcun</p>
					</div>
				</div>

				<div class="row">
					<h2>LaterJS 101</h2>
					<div class="column10 center">
						<p>This simple FW uses scope to the HTML document via data attributes: <code>data-ljs-s</code>. Every document element with this attribute is parsed as documents' section and treated independently from the other sections.</p>
						<p>Every section is a self-sufficient closure that has three main sub-closures: view, call and ctrl.</p>
						<br>
						<p>A <strong>view</strong> closure operates directly with a HTML documents' DOM. Parses the DOM and replaces the inline LaterJS snippets with a corresponding variable values.</p>
						<p>A <strong>ctrl</strong> closure operates with the variables themselves. Every inline LaterJS snippet is parsed and its name is used as name of a newly created variable that exist in local scope of a section. Those variables can be changed, erased and updated either directly via javascript code or through callback functions defined in...</p>
						<p>... a <strong>call</strong> closure. Via this closure can be un/registered and managed all the callback functions that are called whether any change to the sections' variable is made. Callback functions can be binded either to the section as is or to the speciffic section's variable.</p>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>1. Start simple</h2>
					<div class="column5">
						<p>When you try to type into this input field, the same content will be parsed and sent to HTML template to the target place.</p>
						<p>Every input element has native event binding to input.</p>
						<p>This is also great example of using the templating tags. Simply insert into HTML source code a <code>{{variable}}</code> string.</p>
						<p>Right after initializing body of page a variable named "variable" will be created into the current section's scope.</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_01">
						<div>Hello <span class="cBlue">{{world}}</span>!</div>
						<input type="text" name="world" value="" class="size10" placeholder="type a 'world' here" />
						<br>
						<br>
						<div>Additionally, a whole textarea content will be placed here:</div>
						<div class="cBlue">{{text}}</div>
						<textarea name="text" class="size10" placeholder="Type something here..."></textarea>
						<br>
						<br>
						<div>The same can be applied for range elements:</div>
						<span>0</span>
						<input type="range" name="range.x" class="size8" min="0" max="10" />
						<span>{{range.x}} / 10</span>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>2. Let's try radio buttons</h2>
					<div class="column5">
						<p>The radio and check buttons work almost identically. However, radio buttons are easier to understand, as there can be only one active value set to the radio button variable.</p>
						<p>When radio button is not clicked, a variable created from radio element has empty value. Right after first click a clicked element's value is set to variable.</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_02">
						<div>Hello <span class="cBlue">{{world}}</span>!</div>
						<input type="radio" name="world" value="heaven" id="ex02_world_1" />
						<label for="ex02_world_1">Heaven</label>
						<input type="radio" name="world" value="hell" id="ex02_world_2" />
						<label for="ex02_world_2">Hell</label>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="row">
					<h2>3. What about checkboxes?</h2>
					<div class="column5">
						<p>Checkboxes are a little bit trickier. As we know, we can click multiple checkboxes.</p>
						<p>As in case of radio buttons, when no element is checked, a default value of checkbox-created variable is an empty string.</p>
						<p>If there is ony one checked element (in a group of checkboxes with the same name), only a single value is set to the checkbox.</p>
						<p>Otherwise an array is created, containing all the checked values.</p>
						<p>As you can see, all written values are written just as you check and uncheck them.</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_03">
						<div>Let's check them! <span class="cBlue">{{test}}</span></div>
						<input type="checkbox" name="test[]" value="heaven" id="ex03_test_1" />
						<label for="ex03_test_1">Heaven</label>
						<input type="checkbox" name="test[]" value="purgatory" id="ex03_test_2" />
						<label for="ex03_test_2">Purgatory</label>
						<input type="checkbox" name="test[]" value="hell" id="ex03_test_3" />
						<label for="ex03_test_3">Hell</label>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>4. Aaand select box</h2>
					<div class="column5">
						<p>The most complex part of form elements is a select element.</p>
						<p>Selects can be either with single or multiple choices for selection.</p>
						<p>When single selection is available, only a single value is set to the variable.</p>
						<p>Otherwise, just like in checkboxes, an array of selected values is created.</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_04">
						<div>Choose some. <span class="cBlue">{{test}}</span></div>
						<select name="test">
							<option value="">- fruit -</option>
							<option value="1">Berries</option>
							<option value="2">Mango</option>
							<option value="3">Banana</option>
							<option value="4">Apple</option>
						</select>

						<div>Choose more. <span class="cBlue">{{test2}}</span></div>
						<select name="test2" multiple>
							<option value="1">Berries</option>
							<option value="2">Mango</option>
							<option value="3">Banana</option>
							<option value="4">Apple</option>
						</select>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>5. Element attributes</h2>
					<div class="column5">
						<p>Some attributes contain data that can interfere with other parts of code or can cause an Error.</p>
						<p>For example, when you dynamically enter the <code>src</code> attribute to the image element.</p>
						<p>That's when a later's attributes come to play!</p>
						<p>Simply set prefix to the attribute and the whole attribute will be processed during initialization. No more errors!</p>
					</div>
					<div class="column 4 offset1" data-ljs-s="example_05">
						<img class="xxx" ljs-src="{{imagePath}}" width="330" height="90" />
						<p>This image was loaded with a 3 second delay. Check the code.</p>
						<button type="button" class="button" data-ljs-l="changeImage">Change Image</button>
					</div>
				</div>

				<div class="row">
					<h2>6. Adding some callbacks</h2>
					<div class="column5">
						<p>In LaterJS you can bind any anonymous function as callback, that will be triggered in every update of the section.</p>
						<p>If you define a variable scope of a section, a callback function will be binded only to the change of specified variable.</p>
						<p>In the following example we made a binding function to the checkbox that presets value of readonly-input element. Just try it!</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_06">
						<div>Run callback by checking checkbox</div>
						<input type="text" name="i_like" placeholder="I like LaterJS" readonly />
						<input type="checkbox" name="banana" value="I like banana!" id="ex05_banana" />
						<label for="ex05_banana">nope, I like banana!</label>
						
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>7. Just bindings</h2>
					<div class="column5">
						<p>As we can see, all standard form elements and variables too can have binded some callback functions when an eventListener is triggered. But what if I want only to run a script?</p>
						<p>It would be really stupid to create variable just to assign a callback functions to it.</p>
						<p>Just for that reason there are available listeners!</p>
						<p>Listeners are defined via <code>data-ljs-l</code> attribute. Then standard eventListener will be assigned to them and when triggered, they run a list of functions defined just for them.</p>
					</div>
					<div class="column4 offset1 tac" data-ljs-s="example_07">
						<button data-ljs-l="click_me" class="button cBlue">Click me.</button>
					</div>
					<div class="clear"></div>
				</div>


				<div class="row">
					<h2>8. AJAX in action</h2>
					<div class="column5">
						<p>LaterJS can process a quite complex set of AJAX-related methods. You can load a section's data variables througout AJAX result, load the template and much more.</p>
						<p>in this example you can see, that string written in textarea was set with AJAX post method and result is enriched with some text processed on the server side.</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_08">
						<div class="cBlue">{{ajax_response}}</div>
						<textarea name="text" class="size10" placeholder="Enter some text, that will be sent to server"></textarea>
						<button type="button" data-ljs-l="sender" class="button">Send me!</button>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>9. Filling the content in cycle</h2>
					<div class="column5">
						<p>Sometimes, there is a need to generate a content based on a list of data. For example, list of products in e-shop, orders in restaurant etc. To create such a list of generated HTML content there is a simple snippet: <code>{{template in list}}</code>.</p>
						<p>See? Simple. It basically says that for each object element in array will be generated a content that uses all the variables stored in this element.</p>
						<p>For a demonstration over there was created a following template:</p>
						<pre>
&lt;template name="example_09"&gt;
	&lt;li&gt;{{value}}.&lt;/li&gt;
&lt;/template&gt;
						</pre>
						<p>Then a snippet was set into a section's DOM tree: <br><code>{{ example_09 in list}}</code>.</p>
						<p>Finally, a list of values was created in JS: <br><code>e8.ctrl.set('list', [{value: 1}, {value: 2}, {value: 3}]);</code>.</p>
						<p>1, 2, 3 and PUFF! A list of &lt;li&gt; elements is created!</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_09">
						<ul>
							{{example_09 in list}}
						</ul>
						<input type="text" name="li_content" />
						<button data-ljs-l="add_li" class="button cBlue">Add a list item</button>
					</div>
					<div class="clear"></div>
				</div>

				<div class="row">
					<h2>10. Putting parent variables into generated content</h2>
					<div class="column5">
						<p>Sometimes we just need to put some other variables from default object into generated list. (Static list of items in each tab, for example.)</p>
						<p>That's where the <strong>at</strong> prefix comes to play!</p>
						<p>When defining the <em>snippet</em>, just put a "@" character before the variable that you want to grab from default object.</p>
						<p>Variable will be then taken using the "absolute path" instead of relative.</p>
						<pre>
{{@parentsList}}
{{template in @parentsList}}
						</pre>
					</div>
					<div class="column4 offset1" data-ljs-s="example_10">
						<h3>Generated list</h3>
						<ul>
							{{example_10 in list}}
						</ul>
						<button class="button cBlue" data-ljs-l="add_li">New list item</button>
					</div>
				</div>

				<div class="row">
					<h2>11. Calling array values and keys</h2>
					<div class="column5">
						<p>Sometimes you need to draw values of a simple associative array. As such array does not have any named keys to be called with, you can use a <em>#key</em> to draw a key value or <em>#value</em> to output current array's value. Neat!</p>
					</div>
					<div class="column4 offset1" data-ljs-s="example_11">
						<p>Approaching to keys and values of the associative array:</p>
						<code>[1, 2, 'three', 'four', 5]</code>
						<ul>{{example_11__item in list}}</ul>
					</div>
				</div>
			</div>
		</div>

		<footer class="tac">
			created by <a href="http:jocho.sk" target="_blank">Jocho</a> &copy; 2015
		</footer>

		<template name="example_09">
			<li class="{{css}}"><span>{{value}}</span> <button data-ljs-l="remove_li" class="button cRed">&times;</button></li>
		</template>

		<template name="example_10">
			<li>
				<div>This is value of this list's item (relative variable): <strong>{{idx}}</strong></div>
				<div>Value from 1st item of example 8 (absolute variable): <strong>{{@e9List.0.value}}</strong>.</div>
				<div>Here is the list taken from parent object, not list's item:</div>
				<ul>{{example_10__item in @e9List}}</ul>
				<button data-ljs-l="remove_li" class="button cRed">&times;</button></li>
		</template>

		<template name="example_10__item">
			<li>{{value}}</li>
		</template>

		<template name="example_11__item">
			<li><span>{{#key}}</span> - <span>{{#value}}</span></li>
		</template>
	</body>
</html>