var app = new Later();

// just an initialization of section in intro to get current version of LaterJS. Please ignore that :)
var intro = app.section('intro');
intro.ctrl.set('version', app.info()['version']);

/*	Initialization of examples
	Variable assigned like this becomes an instance of automatically generated and registered section in DOM, thanks to data-ljs identifier.
*/
var e1 = app.section('example_01');
var e2 = app.section('example_02');
var e3 = app.section('example_03');
var e4 = app.section('example_04');
var e5 = app.section('example_05');
var e6 = app.section('example_06');
var e7 = app.section('example_07');
var e8 = app.section('example_08');
var e9 = app.section('example_09');
var e10 = app.section('example_10');
var e11 = app.section('example_11');


/*	Example 5
	In this example we show little feature - replacing attributes with "ljs-" prefix after render phase.
	Thanks to this feature we can prevent errors that cause elements like images, videos or other content,
	when it has invalid attribute value given (i.e. empty value).
*/
setTimeout(function() {
	e5.call.run('changeImage', '_');
}, 3000);

e5.ctrl.set({
	imagePath: '',
	activeImage: 0,
	paths: ['image-1.jpg', 'image-2.jpg']
});

e5.call.register('changeImage', '_', function() {
	var paths = e5.ctrl.get('paths');
	var idx = parseInt(e5.ctrl.get('activeImage'));
	idx = (idx) ? 0 : 1;

	e5.ctrl.set('imagePath', paths[idx]);
	e5.ctrl.set('activeImage', idx);
});

/*	Example 6
	This is a simple example of how to assign a callback function to a variable's change.
	Like you can see, we registered here a "bananize" function to "banana" variable. 
	Callback function itself doesn't do much; it only sets a value to another variable, named "i_like".
*/
e6.call.register('banana', '_', function() {
	e6.ctrl.set('i_like', e6.ctrl.get('banana'));
});

/*	Example 7
	Defining a simple listener can spare us a lot of overdefined variables with no value.
*/
e7.call.register('click_me', 'one', function(x) {
	alert('This button initialized me! "' + x + '"');
});

e7.call.register('click_me', 'two', function(x) {
	alert('Surprised? Yeah, you can bind more callback and listener functions into a flow.');
});

/*	Example 7
	This is an AJAX example. Here we register a function binded to a "sender" variable. This function
	sends a data stored within a section and returned value (processed via server) is set to a secion's variable.
*/
e8.ctrl.set('ajax_response', '');
e8.call.register('sender', 'ajax', function() {
	e8.send('put', function(result) {
		var res = JSON.parse(result);
		e8.ctrl.set('ajax_response', res);
	});
});

/*	Example 8
	This is a final example - a simple application of dynamically generated list. 
	Firstly the initial list is generated. Then the two callback functions are binded - one stores written data
	and another deletes the current list item.

	The point of all of this is to show that the DOM is automatically regenerated based on items stored in a variable,
	that is binded to this list view.
*/
e9.ctrl.set('list', [
	{value: 'Hello', css: 'ok'},
	{value: 'I am', css: 'no'},
	{value: 'a generated list!', css: 'ok'}
]);

e9.call.register('add_li', 'add', function(v) {
	var list = e9.ctrl.get('list');
	list.push({value: e9.ctrl.get('li_content'), css: 'yeah'});
	e9.ctrl.set('list', list);
	e9.ctrl.set('li_content', '');

	e10.ctrl.set('e9List', list);
});
e9.call.register('+remove_li', 'remove', function(li) {
	var list = e9.ctrl.get('list');
	list.splice(li[1], 1);
	e9.ctrl.set('list', list);

	e10.ctrl.set('e9List', list);
});

e10.ctrl.set({
	e9List: e9.ctrl.get('list'),
	list: []
});

e10.call.register('add_li', '_', function(li) {
	var list = e10.ctrl.get('list');
	list.push({idx: list.length + 1});
	e10.ctrl.set('list', list);
});

e10.call.register('+remove_li', '_', function(li) {
	var list = e10.ctrl.get('list');
	list.splice(li[1], 1);
	e10.ctrl.set('list', list);
});


e11.ctrl.set('list', [1, 2, 'three', 'four', 5]);